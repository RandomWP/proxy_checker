import argparse
import requests
import typing
import colorama

CHK_ADDR = ("google.com", )


def filter_proxies(proxies: typing.Iterable, timeout, types: tuple, dry: bool, g_file, b_file, sum_method=any):
    for n, proxy in enumerate(proxies):
        print(f"{n + 1}. Checking proxy:", proxy, end="\t", flush=True)
        check_res = check_proxy(proxy, timeout, types)
        summed = sum_method(check_res.values())
        print((colorama.Fore.GREEN if summed else colorama.Fore.RED) +
              ("GOOD" if summed else "BAD") + colorama.Fore.RESET)
        print("Result:", *map(lambda item: f"{item[0]} - {colorama.Fore.GREEN + 'YES' if item[1] else colorama.Fore.RED + 'NO'}"
                              + colorama.Fore.RESET, check_res.items()))
        if not dry:
            if summed:
                print(proxy, file=g_file)
            else:
                print(proxy, file=b_file)


def check_proxy(proxy: str, timeout, types: tuple):
    proxies = {t: f"{t}://{proxy}/" for t in types}
    result = {}
    for address in CHK_ADDR:
        for t in types:
            url = f"{t}://{address}/"
            try:
                response = requests.get(url, proxies=proxies, timeout=timeout)
                result[t] = bool(response)
            except Exception:
                result[t] = False
    return result


def load_lines(file_path: str) -> tuple:
    with open(file_path, "r") as f:
        lines = f.readlines()
    lines = tuple(map(lambda line: line.strip(), lines))
    return lines


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--dry", action="store_true",
                        help="Write info in terminal, do not create any files")
    parser.add_argument("-p", "--protocol", action="store",
                        help="Specify protocol (https, http) (default: all)", choices=("https", "http"))
    parser.add_argument("-t", "--timeout", type=float, default=10)
    parser.add_argument("proxy", type=str,
                        help="Path to file with proxies", metavar="PROXY")
    parser.add_argument("good", type=str, nargs="?", help="Path to file where to save good proxies",
                        default="good.txt", metavar="GOOD")
    parser.add_argument("bad", nargs="?", type=str, help="Path to file where to save bad proxies (to use with -b)",
                        default="bad.txt", metavar="BAD")
    args = parser.parse_args()
    proxies = load_lines(args.proxy)
    if args.protocol:
        types = (args.protocol,)
    else:
        types = ("https", "http")
    with open(args.good, "w") as g_file:
        with open(args.bad, "w") as b_file:
            filter_proxies(proxies, types=types, timeout=args.timeout,
                           dry=args.dry, g_file=g_file, b_file=b_file)


if __name__ == "__main__":
    main()
